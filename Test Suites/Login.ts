<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>b2a94848-649c-45b2-a8a5-6ae69e4e36b7</testSuiteGuid>
   <testCaseLink>
      <guid>b8084c20-6dbf-437d-8696-7ea6b507480d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG1 - User valid</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8ec10bea-8046-434b-8984-b0fea49740f3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data Positive</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>8ec10bea-8046-434b-8984-b0fea49740f3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Nama</value>
         <variableId>d4bc0cab-57cf-4764-b058-83370421f4c9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8ec10bea-8046-434b-8984-b0fea49740f3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>950e46fb-c7f9-4eb0-991c-15752885f60d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8a68defc-8c7b-4d49-8522-260b026f65b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG2 - User tidak valid</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2e29889c-94d3-46fd-af66-99a086bc0726</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data Negative</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>2e29889c-94d3-46fd-af66-99a086bc0726</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Nama</value>
         <variableId>4750b9c0-d092-440d-92e7-0997d95c188b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2e29889c-94d3-46fd-af66-99a086bc0726</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>7ecc6862-d40c-445d-b9a5-0bacfb687a17</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>28b30dd9-7edb-4a94-a5af-f94ba52a62f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LOG3 - Username kosong</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d76c8931-b297-4bf8-8754-4c880b2b844f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data Negative (user kosong)</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>d76c8931-b297-4bf8-8754-4c880b2b844f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Nama</value>
         <variableId>d3e197fc-8727-44c5-aa09-68aadc3ea163</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d76c8931-b297-4bf8-8754-4c880b2b844f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>5cb53626-c70a-4251-b4d3-12c3a7c15b41</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
